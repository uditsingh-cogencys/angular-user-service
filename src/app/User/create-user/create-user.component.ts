import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/Model/user.model';
import { UserService } from 'src/app/Service/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  user:User= new User();
  submitted=false;
  constructor(private userService:UserService,private route: ActivatedRoute,  private router:Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.submitted=true;
    this.userService.createUser(this.user).subscribe(result => this.gotoUserList());
  }
  gotoUserList(){
    this.router.navigate(['/users']);
  }
}

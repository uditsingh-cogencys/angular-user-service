import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from 'src/app/Model/user.model';
import { UserService } from 'src/app/Service/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users!: User[];
  constructor(private userService:UserService,private router:Router) { }

  ngOnInit(): void {
    this.userService.findAll().subscribe(data => {
      this.users = data;
    });
  }
  deleteUser(userId:number){
     this.userService.deleteUser(userId).subscribe( 
       data=>{this.userService.findAll().subscribe(data => {
          this.users = data;
       });
      },
      result => this.gotoUserList());
    }
    gotoUserList(){
      this.router.navigate(['/users']);
    }
  
  updateUser(user:User){
    this.router.navigate(['update',user]);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiResponse } from 'src/app/Model/api.response';
import { User } from 'src/app/Model/user.model';
import { UserService } from 'src/app/Service/user.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  userId!:number;
  user!:User;
  apiResponse!:ApiResponse;
  constructor(private router:Router,private userService:UserService,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.user=new User();
    this.userId=this.route.snapshot.params['userId'];
    this.userService.getUserById(this.userId).subscribe(
      data=>{console.log(data);
      this.user=data;
    },
    error=>console.error(error));
          
    
  }
  onSubmit(){
    this.userService.updateUser(this.user).subscribe(
      data=>console.log(data),error=>console.error(error));
      this.user=new User();
      this.router.navigate(['/users'])
    
  }

}

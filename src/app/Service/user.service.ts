import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { ApiResponse } from '../Model/api.response';
import { User } from '../Model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  baseUrl: string='http://localhost:7070/api/user/';

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }
  getUsers() : Observable<User[]>{
    return this.http.get<User[]>(this.baseUrl);
  }
  getUserById(userId:number):Observable<any>{
    return this.http.get(this.baseUrl + userId);
  }
  createUser(user: User) {
    return this.http.post<User>(this.baseUrl, user);
  }
  updateUser(user:User):Observable<ApiResponse>{
    return this.http.put<ApiResponse>(this.baseUrl,user);
  }
  deleteUser(userId:number):Observable<User>{
    return this.http.delete<User>(this.baseUrl+userId);
  }
}

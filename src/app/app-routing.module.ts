import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUserComponent } from './User/create-user/create-user.component';
import { UpdateUserComponent } from './User/update-user/update-user.component';
import { UserListComponent } from './User/user-list/user-list.component';

const routes: Routes = [
  {path:'',redirectTo:'user',pathMatch:'full'},
  {path: 'add',component:CreateUserComponent},
  {path: 'users',component:UserListComponent},
  {path: 'update',component:UpdateUserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
